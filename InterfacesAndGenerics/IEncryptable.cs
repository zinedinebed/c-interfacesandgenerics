﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesAndGenerics
{
    interface IEncryptable
    {
        void Encrypt();
        void Decrypt();

        void SomeMethod();
    }
}
