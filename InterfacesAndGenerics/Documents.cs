﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel; // for InotifyPropChanged

namespace InterfacesAndGenerics
{
    class Documents :  IStorable, IEncryptable
    {
        private string name;

        public string DocName
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropChanged("DocName");
                AnoterNotifyPropChanged("DocName : Anotehr notification");
            }
        }

        public Documents(string s)
        {
            name = s;
            Console.WriteLine($"Created a document with name {name}");
        }

        //implement interface IStorable
        private Boolean needSave = false;
        public Boolean NeedSave
        {
            get
            {
                return needSave;
            }
            set
            {
                needSave = value;
                NotifyPropChanged("NeedSave");
                AnoterNotifyPropChanged("NeedSave : Anotehr notification");
            }
        }

        //Event for notify the others about the chenged property
        public event PropertyChangedEventHandler PropertyCHanged;
        public event PropertyChangedEventHandler propChanged;

        public void Load()
        {
            Console.WriteLine("Loading the document ...");
        }

        public void Save()
        {
            Console.WriteLine("Saving the document");
        }

        public void Encrypt()
        {
            Console.WriteLine("Encrypting  the document");
        }

        public void Decrypt()
        {
            Console.WriteLine("Decrypting the document");
        }

        public void SomeMethod()
        {
            Console.WriteLine("This is the document SomeMethod");
        }

        void IStorable.SomeMethod()
        {
            Console.WriteLine("This is the document Storable SomeMethod");
        }

        void IEncryptable.SomeMethod()
        {
            Console.WriteLine("This is the document Encryptable SomeMethod");
        }

        private void NotifyPropChanged(string propName)
        {
            PropertyCHanged(this, new PropertyChangedEventArgs(propName));
        }

        private void AnoterNotifyPropChanged(string name)
        {
            propChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
