﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesAndGenerics
{
    class CustomRandom : IRondomizable
    {
        public double GetRandomNum(double upperBound)
        {
            Random random = new Random();

            double seed = random.NextDouble();

            return seed * upperBound;
        }
    }
}
