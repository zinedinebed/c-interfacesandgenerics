﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace InterfacesAndGenerics
{
    class Program
    {
        static void Main(string[] args)
        {
            //Interfaces Program test

            Documents document = new Documents("Test Document");

            //document.Load();
            //document.Save();
            //document.NeedSave = false;

            if (document is IStorable)
            {
                document.Encrypt();
                document.Save();
            }

            IStorable intStor = document as IStorable; // castin with "as" to ISTORABLE
            if (intStor != null )
            {
                document.Load();
                document.Decrypt();
            }


            Documents document2 = new Documents("Document 2");
            document2.SomeMethod();

            IStorable storable = document2 as IStorable;
            storable.SomeMethod();

            IEncryptable encryptable = document2 as IEncryptable;
            encryptable.SomeMethod();

            //listen for the property changed in document
            document.PropertyCHanged += delegate (object sender, PropertyChangedEventArgs e)
            {
                Console.WriteLine($"Document property changed : {e.PropertyName}");
            };

            //Another  delegate just for practice
            document.propChanged += delegate (object sender, PropertyChangedEventArgs e)
            {
                Console.WriteLine($"Document property changed : {e.PropertyName}");
            };

            document.DocName = "My document";
            document.NeedSave = true;


            CustomRandom customRandom = new CustomRandom();
            Console.WriteLine(customRandom.GetRandomNum(150));

            ///Collections and generics
            List<Employee> employees = new List<Employee>();
            //Implement the Icomparer Interface
            EmployeeComparer employeeComparer = new EmployeeComparer();

            employees.Add(new Employee("Zinedine Bedrani", 90000));
            employees.Add(new Employee("Anis Bedrani", 70000));
            employees.Add(new Employee("Momoh Bedrani", 45000));
            employees.Add(new Employee("Amir Bedrani", 130000));

            //we can specify the capacity of the list in construction , in this case it will 4
            //if we know how many item we will have it's better to specify the number for performance
            Console.WriteLine($"The capacity of the list is : {employees.Capacity}");
            Console.WriteLine($"The count of the list is : {employees.Count}");

            //search element with high pay : we need delegate (predicate in this case) for Exist method
            if (employees.Exists(HighPay))
            {
                Console.WriteLine("Yes, There is an employee with salary > 100000");
            }

            //Find employee which name start with Z
            Employee employee = employees.Find(x => x.mName.StartsWith("Z"));
            if (employee!= null)
            {
                Console.WriteLine($"There is an employee which his name start with with Z : {employee.mName}");
            }

            //calculate total salary
            //interesting use of the foreach
            employees.ForEach(TotalSalary);
            Console.WriteLine($"The total Salary is : {total}");

            //sorting the list depending on salary
            //nice way to sort list of object 
            employees.Sort(employeeComparer);
            foreach (Employee e in employees)
            {
                Console.WriteLine($"{e.mName} , {e.mSalary}");
            }

            //binary literals tests
            int value = 0b0010_0110_0000_0011;
            Console.WriteLine($"This binary litral value  = {value}");

            ////Dictionary Test
            ///
            ///
            Dictionary<string, string> fileType = new Dictionary<string, string>();

            fileType.Add(".txt", "Text File");
            fileType.Add(".html", "HTML file");
            fileType.Add(".htm", "HTML file");
            fileType.Add(".css", "Style Sheet");
            fileType.Add(".xml", "XML Data");

            // how many keys values pairs in dictionary
            Console.WriteLine($"There are {fileType.Count} pairs in fileType Dictionary");

            //Try to add an existing key
            /// Add method will throw an exception : lets hadle it 
            /// 
            try
            {
                fileType.Add(".html", "HTML ");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Key already exist in the Dictionary");
                Console.WriteLine(e.Message);
            }

            //remove a key from a dictionary
            fileType.Remove(".css");
            Console.WriteLine($"Is there a key = to '.css' ? : {fileType.ContainsKey(".css")}  ");


        }

        //iterator function for calculatin the totalsalary
        static int total;
        private static void TotalSalary(Employee obj)
        {
            total += obj.mSalary;
        }

        //delegate function to use with Eist method for list 
        //check if there is an employee with salary > 100000
        private static bool HighPay(Employee obj)
        {
            return obj.mSalary > 100000;
        }


    }
}
