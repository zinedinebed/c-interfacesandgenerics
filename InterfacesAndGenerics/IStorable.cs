﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesAndGenerics
{
    interface IStorable
    {
        void Load();
        void Save();

        Boolean NeedSave { get; set; }
  
        void SomeMethod();
    }
}
