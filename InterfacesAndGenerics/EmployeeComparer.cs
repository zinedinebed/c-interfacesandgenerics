﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesAndGenerics
{
    class EmployeeComparer : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            if (x.mSalary > y.mSalary)
            {
                return 1;
            }
            
            if (x.mSalary == y.mSalary)
            {
                return 0;
            }
            else
            {
                return -1;
            }

        }
    }
}
